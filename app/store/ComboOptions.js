Ext.define('App.store.ComboOptions', {
	extend: 'Ext.data.Store',
	model: 'App.model.ComboOption',
	data: [
		{ 'id': 1, 'name': 'Guy Todoh'},
		{ 'id': 2, 'name': 'Cody Travers'}	
	]
});	